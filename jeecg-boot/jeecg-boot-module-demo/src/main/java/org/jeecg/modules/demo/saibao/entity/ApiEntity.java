package org.jeecg.modules.demo.saibao.entity;

import lombok.Data;

@Data
public class ApiEntity {

    private String value;
    private String name;
    private String date;

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}
