package org.jeecg.modules.demo.saibao.service;

import org.jeecg.modules.demo.saibao.entity.SaibaoCertificate;
import com.baomidou.mybatisplus.extension.service.IService;
import java.util.List;

/**
 * @Description: 证书
 * @Author: jeecg-boot
 * @Date:   2021-06-19
 * @Version: V1.0
 */
public interface ISaibaoCertificateService extends IService<SaibaoCertificate> {

	public List<SaibaoCertificate> selectByMainId(String mainId);
}
