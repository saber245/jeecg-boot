package org.jeecg.modules.demo.saibao.service.impl;

import org.jeecg.modules.demo.saibao.entity.SaibaoCertificate;
import org.jeecg.modules.demo.saibao.mapper.SaibaoCertificateMapper;
import org.jeecg.modules.demo.saibao.service.ISaibaoCertificateService;
import org.springframework.stereotype.Service;
import java.util.List;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @Description: 证书
 * @Author: jeecg-boot
 * @Date:   2021-06-19
 * @Version: V1.0
 */
@Service
public class SaibaoCertificateServiceImpl extends ServiceImpl<SaibaoCertificateMapper, SaibaoCertificate> implements ISaibaoCertificateService {
	
	@Autowired
	private SaibaoCertificateMapper saibaoCertificateMapper;
	
	@Override
	public List<SaibaoCertificate> selectByMainId(String mainId) {
		return saibaoCertificateMapper.selectByMainId(mainId);
	}
}
