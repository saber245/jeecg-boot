package org.jeecg.modules.demo.saibao.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.jeecg.modules.demo.saibao.entity.ApiEntity;
import org.jeecg.modules.demo.saibao.entity.SaibaoProManage;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Description: 项目管理
 * @Author: jeecg-boot
 * @Date:   2021-06-19
 * @Version: V1.0
 */
public interface SaibaoProManageMapper extends BaseMapper<SaibaoProManage> {
    List<ApiEntity> getCountByMonth();

    List<ApiEntity> getCountByCity();

    List<ApiEntity> getCountByLevel();

    List<ApiEntity> getCountByState();

    String getTotalCount(String state);
}
