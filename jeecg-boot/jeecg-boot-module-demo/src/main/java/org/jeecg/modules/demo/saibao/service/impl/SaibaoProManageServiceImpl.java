package org.jeecg.modules.demo.saibao.service.impl;

import org.jeecg.modules.demo.saibao.entity.ApiEntity;
import org.jeecg.modules.demo.saibao.entity.SaibaoProManage;
import org.jeecg.modules.demo.saibao.entity.SaibaoCertificate;
import org.jeecg.modules.demo.saibao.mapper.SaibaoCertificateMapper;
import org.jeecg.modules.demo.saibao.mapper.SaibaoProManageMapper;
import org.jeecg.modules.demo.saibao.service.ISaibaoProManageService;
import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import java.io.Serializable;
import java.util.List;
import java.util.Collection;

/**
 * @Description: 项目管理
 * @Author: jeecg-boot
 * @Date:   2021-06-19
 * @Version: V1.0
 */
@Service
public class SaibaoProManageServiceImpl extends ServiceImpl<SaibaoProManageMapper, SaibaoProManage> implements ISaibaoProManageService {

	@Autowired
	private SaibaoProManageMapper saibaoProManageMapper;
	@Autowired
	private SaibaoCertificateMapper saibaoCertificateMapper;

	@Override
	public List<ApiEntity> getCountByMonth() {
		return saibaoProManageMapper.getCountByMonth();
	}

	@Override
	public List<ApiEntity> getCountByCity() {
		return saibaoProManageMapper.getCountByCity();
	}

	@Override
	public List<ApiEntity> getCountByLevel() {
		return saibaoProManageMapper.getCountByLevel();
	}

	@Override
	public List<ApiEntity> getCountByState() {
		return saibaoProManageMapper.getCountByState();
	}

	@Override
	public String getTotalCount(String state) {
		return saibaoProManageMapper.getTotalCount(state);
	}

	@Override
	@Transactional
	public void saveMain(SaibaoProManage saibaoProManage, List<SaibaoCertificate> saibaoCertificateList) {
		saibaoProManageMapper.insert(saibaoProManage);
		if(saibaoCertificateList!=null && saibaoCertificateList.size()>0) {
			for(SaibaoCertificate entity:saibaoCertificateList) {
				//外键设置
				entity.setProId(saibaoProManage.getId());
				saibaoCertificateMapper.insert(entity);
			}
		}
	}

	@Override
	@Transactional
	public void updateMain(SaibaoProManage saibaoProManage,List<SaibaoCertificate> saibaoCertificateList) {
		saibaoProManageMapper.updateById(saibaoProManage);

		//1.先删除子表数据
		saibaoCertificateMapper.deleteByMainId(saibaoProManage.getId());

		//2.子表数据重新插入
		if(saibaoCertificateList!=null && saibaoCertificateList.size()>0) {
			for(SaibaoCertificate entity:saibaoCertificateList) {
				//外键设置
				entity.setProId(saibaoProManage.getId());
				saibaoCertificateMapper.insert(entity);
			}
		}
	}

	@Override
	@Transactional
	public void delMain(String id) {
		saibaoCertificateMapper.deleteByMainId(id);
		saibaoProManageMapper.deleteById(id);
	}

	@Override
	@Transactional
	public void delBatchMain(Collection<? extends Serializable> idList) {
		for(Serializable id:idList) {
			saibaoCertificateMapper.deleteByMainId(id.toString());
			saibaoProManageMapper.deleteById(id);
		}
	}

}
