package org.jeecg.modules.demo.saibao.mapper;

import java.util.List;
import org.jeecg.modules.demo.saibao.entity.SaibaoCertificate;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

/**
 * @Description: 证书
 * @Author: jeecg-boot
 * @Date:   2021-06-19
 * @Version: V1.0
 */
public interface SaibaoCertificateMapper extends BaseMapper<SaibaoCertificate> {

	public boolean deleteByMainId(@Param("mainId") String mainId);
    
	public List<SaibaoCertificate> selectByMainId(@Param("mainId") String mainId);
}
