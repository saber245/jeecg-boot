package org.jeecg.modules.demo.saibao.vo;

import java.util.List;
import org.jeecg.modules.demo.saibao.entity.SaibaoProManage;
import org.jeecg.modules.demo.saibao.entity.SaibaoCertificate;
import lombok.Data;
import org.jeecgframework.poi.excel.annotation.Excel;
import org.jeecgframework.poi.excel.annotation.ExcelEntity;
import org.jeecgframework.poi.excel.annotation.ExcelCollection;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;
import java.util.Date;
import org.jeecg.common.aspect.annotation.Dict;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * @Description: 项目管理
 * @Author: jeecg-boot
 * @Date:   2021-06-19
 * @Version: V1.0
 */
@Data
@ApiModel(value="saibao_pro_managePage对象", description="项目管理")
public class SaibaoProManagePage {

	/**主键*/
	@ApiModelProperty(value = "主键")
    private String id;
	/**客户单位*/
	@Excel(name = "客户单位", width = 15)
	@ApiModelProperty(value = "客户单位")
    private String customerCompany;
	/**城市*/
	@Excel(name = "城市", width = 15)
	@ApiModelProperty(value = "城市")
    private String city;
	/**申请级别*/
	@Excel(name = "申请级别", width = 15, dicCode = "application_level")
    @Dict(dicCode = "application_level")
	@ApiModelProperty(value = "申请级别")
    private String applicationLevel;
	/**项目状态*/
	@Excel(name = "项目状态", width = 15, dicCode = "pro_state")
    @Dict(dicCode = "pro_state")
	@ApiModelProperty(value = "项目状态")
    private String state;
	/**建档时间*/
	@Excel(name = "建档时间", width = 15, format = "yyyy-MM-dd")
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern="yyyy-MM-dd")
	@ApiModelProperty(value = "建档时间")
    private Date fileDate;
	/**商务负责人*/
	@Excel(name = "商务负责人", width = 15)
	@ApiModelProperty(value = "商务负责人")
    private String businessDirector;
	/**项目负责人*/
	@Excel(name = "项目负责人", width = 15)
	@ApiModelProperty(value = "项目负责人")
    private String projectDirector;
	/**企业对接人*/
	@Excel(name = "企业对接人", width = 15)
	@ApiModelProperty(value = "企业对接人")
    private String customerDirector;
	/**渠道对接人*/
	@Excel(name = "渠道对接人", width = 15)
	@ApiModelProperty(value = "渠道对接人")
    private String channelDirector;
	/**首次/启动会时间*/
	@Excel(name = "首次/启动会时间", width = 15, format = "yyyy-MM-dd")
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern="yyyy-MM-dd")
	@ApiModelProperty(value = "首次/启动会时间")
    private Date firstDate;
	/**文审提交时间*/
	@Excel(name = "文审提交时间", width = 15, format = "yyyy-MM-dd")
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern="yyyy-MM-dd")
	@ApiModelProperty(value = "文审提交时间")
    private Date secondDate;
	/**审前培训时间*/
	@Excel(name = "审前培训时间", width = 15, format = "yyyy-MM-dd")
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern="yyyy-MM-dd")
	@ApiModelProperty(value = "审前培训时间")
    private Date preVerifyDate;
	/**审核时间*/
	@Excel(name = "审核时间", width = 15, format = "yyyy-MM-dd")
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern="yyyy-MM-dd")
	@ApiModelProperty(value = "审核时间")
    private Date verifyDate;
	/**备注*/
	@Excel(name = "备注", width = 15)
	@ApiModelProperty(value = "备注")
    private String remark;
	/**创建人*/
	@ApiModelProperty(value = "创建人")
    private String createBy;
	/**创建日期*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
	@ApiModelProperty(value = "创建日期")
    private Date createTime;
	/**更新人*/
	@ApiModelProperty(value = "更新人")
    private String updateBy;
	/**更新日期*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
	@ApiModelProperty(value = "更新日期")
    private Date updateTime;
	/**所属部门*/
	@ApiModelProperty(value = "所属部门")
    private String sysOrgCode;

	@ExcelCollection(name="证书")
	@ApiModelProperty(value = "证书")
	private List<SaibaoCertificate> saibaoCertificateList;

}
