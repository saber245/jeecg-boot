package org.jeecg.modules.demo.saibao.service;

import org.jeecg.modules.demo.saibao.entity.ApiEntity;
import org.jeecg.modules.demo.saibao.entity.SaibaoCertificate;
import org.jeecg.modules.demo.saibao.entity.SaibaoProManage;
import com.baomidou.mybatisplus.extension.service.IService;
import java.io.Serializable;
import java.util.Collection;
import java.util.List;

/**
 * @Description: 项目管理
 * @Author: jeecg-boot
 * @Date:   2021-06-19
 * @Version: V1.0
 */
public interface ISaibaoProManageService extends IService<SaibaoProManage> {

	/**
	 * 添加一对多
	 *
	 */
	public void saveMain(SaibaoProManage saibaoProManage,List<SaibaoCertificate> saibaoCertificateList) ;

	/**
	 * 修改一对多
	 *
	 */
	public void updateMain(SaibaoProManage saibaoProManage,List<SaibaoCertificate> saibaoCertificateList);

	/**
	 * 删除一对多
	 */
	public void delMain (String id);

	/**
	 * 批量删除一对多
	 */
	public void delBatchMain (Collection<? extends Serializable> idList);

	public List<ApiEntity> getCountByMonth();

	public List<ApiEntity> getCountByCity();

	public List<ApiEntity> getCountByLevel();

	public List<ApiEntity> getCountByState();

	public  String getTotalCount(String state);

}
