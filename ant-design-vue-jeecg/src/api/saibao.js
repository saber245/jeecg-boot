import api from './index'
import { axios } from '@/utils/request'


export function getCountByMonth(parameter) {
  return axios({
    url: '/saibao/saibaoProManage/getByMonth',
    method: 'get',
    data: parameter
  })
}

export function getCountByCity(parameter) {
  return axios({
    url: '/saibao/saibaoProManage/getByCity',
    method: 'get',
    data: parameter
  })
}

export function getCountByState(parameter) {
  return axios({
    url: '/saibao/saibaoProManage/getByState',
    method: 'get',
    data: parameter
  })
}

export function getCountByLevel(parameter) {
  return axios({
    url: '/saibao/saibaoProManage/getByLevel',
    method: 'get',
    data: parameter
  })
}

export function getTotalCount(parameter) {
  return axios({
    url: '/saibao/saibaoProManage/getTotalCount',
    method: 'get',
    data: parameter
  })
}